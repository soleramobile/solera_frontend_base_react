import React from 'react';
import { hydrate } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { AuthProvider } from './providers/AuthProvider';

import App from './App';

const Main: React.FC = () => (
  <AuthProvider>
    <App />
  </AuthProvider>
)

hydrate(
  <BrowserRouter>
    <Main />
  </BrowserRouter>,
  document.getElementById('root')
);

if (module.hot) {
  module.hot.accept();
}
