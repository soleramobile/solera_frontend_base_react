export type State = {
  isLoggedIn: boolean;
  user: object;
};

export type Actions =
  | { type: 'LOGIN'; user: object }
  | { type: 'LOGOUT'; user: object }

export const authReducer = (state: State, action: Actions) => {
  switch (action.type) {
    case 'LOGIN':
      return { isLoggedIn: true, user: action.user };
    case 'LOGOUT':
      return { isLoggedIn: false, user: {} };
    default:
      throw new Error();
  };
};
