const colors = {
  primary: '#ffa019',
  secondary: '#2d40d8',
  white: '#ffffff',
  black: '#000000',
  gray: '#595959'
};

const space = [0, 4, 8, 16, 32, 64, 128, 256, 512];

const breakpoints = ['40em', '52em', '64em', '80em']

export default {
  colors,
  space,
  breakpoints,
};
