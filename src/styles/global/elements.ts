import { css } from 'styled-components'

const elements = css`
  * {
    box-sizing: border-box;
  }

  html,
  body {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 16px;
    font-weight: normal;
    font-style: normal;
    line-height: 1.25;
    font-synthesis: none;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p,
  ul,
  li,
  div {
    line-height: 1.25;
  }

  p {
    margin-bottom: 1.15em;

    &:last-of-type {
      margin-bottom: 0;
    }
  }

  a {
    text-decoration: none;
  }

  a,
  button {
    cursor: pointer;
    transition: all 200ms ease;
  }

  button {
    border: 0;
    outline: 0;
    padding: 0;
    text-align: center;
    box-shadow: none;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    justify-content: center;
    background-color: $transparent;
    transition: all 250ms ease;
  }
    
  img {
    max-width: 100%;
    display: block;
    height: auto;
  }
    
  ul {
    list-style-type: none;
  }
    
  button,
  input,
  select,
  textarea,
  label,
  a {
    outline: 0;
    box-shadow: none;
    -webkit-tap-highlight-color: $transparent;
    
    &:focus {
      outline: 0;
      box-shadow: none;
    }
  }

  textarea {
    height: auto !important;
    resize: none;
  }
`
export default elements
