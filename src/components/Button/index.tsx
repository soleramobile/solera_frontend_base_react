import React from "react";
import styled, { css } from "styled-components";
import { space, color, layout } from "styled-system";

interface Props extends React.ComponentPropsWithoutRef<"button"> {
  primary?: boolean;
  secondary?: boolean;
}

const StyledButton = styled.button<Props>`
  cursor: pointer;
  padding: .75rem 1rem;
  border-radius: .25rem;
  width: 100%;
  color: #fff;
  ${color}
  ${space}
  ${layout}
`;

const Button = React.forwardRef<HTMLButtonElement, Props>((props, ref) => (
  <StyledButton ref={ref} {...props}>
    {props.children}
  </StyledButton>
));

export default Button;