import React from 'react';
import styled from 'styled-components';

interface Props extends React.ComponentPropsWithoutRef<'input'> {}

const StyledInput = styled.input`
  font-size: 1rem;
  padding: .75rem 1.25rem;
  background-color: #fff;
  border-radius: .25rem;
  border: 1px solid #ccc;
  box-shadow: none;
  width: 100%;
`;

const TextField = React.forwardRef<HTMLInputElement, Props>((props, ref) => (
  <StyledInput
    type="text"
    ref={ref}
    {...props}
  />
));

export default TextField;
