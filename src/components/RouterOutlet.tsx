import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import Layout from './Layout';
import { AuthStateContext } from '../providers/AuthProvider';

type Props = {
  path: string;
  name: string,
  component: React.ReactNode;
  isProtected: boolean;
}

const defaultProps = {
  isProtected: true,
}

const RouterOutlet: React.FC<Props> = (route) => {
  const { isLoggedIn } = useContext(AuthStateContext);

  const render = (props: JSX.IntrinsicAttributes) => {
    const component = (
      <route.component
        {...props}
        config={{
          name: route.name,
        }}
      />
    );

    if (route.isProtected) {
      return (isLoggedIn
        ? component
        : <Redirect to={{ pathname: '/' }} />);
    }

    return component;
  }

  return <Route path={route.path} render={render} />
}

RouterOutlet.defaultProps = defaultProps;

export default RouterOutlet;
