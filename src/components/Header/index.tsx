import React from 'react';
import { Link } from 'react-router-dom';

const Header: React.FC = () => {
  return (
    <header>
      <Link to="/home">
        Logo
      </Link>
    </header>
  )
}

export default Header;
