import Login from './modules/Auth';
import Home from './modules/Home';

const routes = [
  ...Login,
  ...Home
];

export default routes;
