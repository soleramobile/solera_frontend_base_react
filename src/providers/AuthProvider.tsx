import React, { useReducer, createContext, Dispatch } from 'react';
import { authReducer, State, Actions } from '../reducers/authReducer';

const initialState = {
  isLoggedIn: false,
  user: {},
};

export const AuthStateContext = createContext<Partial<State>>({});
export const AuthDispatchContext = createContext<Dispatch<Actions>>(() => {});

export const AuthProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, initialState);

  return (
    <AuthStateContext.Provider value={state}>
      <AuthDispatchContext.Provider value={dispatch}>
        {children}
      </AuthDispatchContext.Provider>
    </AuthStateContext.Provider>
  )
}
