import styled from 'styled-components';
import { space, layout, color } from 'styled-system';

export const Hero = styled.div`
  background-color: #ccc;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 2rem;
  ${color}
  ${space}
`

export const Form = styled.form`
  ${space}
  ${layout}
`

export const FieldWrap = styled.div`
  margin-bottom: 1rem;
`
