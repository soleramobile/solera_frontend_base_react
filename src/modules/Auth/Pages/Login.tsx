import React, {
  useState, useContext, ChangeEvent, FormEvent
} from 'react';
import { Redirect } from 'react-router-dom';
import TextField from '../../../components/TextField';
import Button from '../../../components/Button';
import { Hero, Form, FieldWrap } from './styles';
import { AuthStateContext, AuthDispatchContext } from '../../../providers/AuthProvider';

type User = {
  email: string;
  password: string;
}

const initialForm = {
  email: '',
  password: '',
};

const Login: React.FC = () => {
  const dispatch = useContext(AuthDispatchContext);
  const { isLoggedIn } = useContext(AuthStateContext);
  const [form, setForm] = useState<User>(initialForm);

  if (isLoggedIn) {
    return <Redirect to={{ pathname: '/home' }} />
  }

  const handleChange = (ev: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = ev.target;
    setForm({ ...form, [name]: value })
  };

  const handleSubmit = (ev: FormEvent) => {
    ev.preventDefault();
    // CallAPI Login
    // If success is true
    dispatch({
      type: 'LOGIN',
      user: {
        name: 'Jimmy Luna',
        email: 'jluna@solera.pe',
      }
    })
  }

  return (
    <>
      <Hero bg="gray" color="white" py={6}>
        Hero
      </Hero>
      <Form mx="auto" p={5} width={1/3} onSubmit={handleSubmit}>
        <FieldWrap>
          <TextField
            type="text"
            name="email"
            value={form?.email}
            placeholder="Ingrese su email"
            required
            onChange={handleChange}
          />
        </FieldWrap>
        <FieldWrap>
          <TextField
            type="password"
            name="password"
            value={form?.password}
            placeholder="Ingrese su correo"
            required
            onChange={handleChange}
          />
        </FieldWrap>
        <FieldWrap>
          <Button type="submit" bg="primary">
            ENVIAR
          </Button>
        </FieldWrap>
      </Form>
    </>
  )
};

export default Login;
