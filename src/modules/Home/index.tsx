import Home from './Pages';

const path = '/home'

const routes = [
  {
    name: 'Home',
    path,
    component: Home,
    exact: true,
  }
]

export default routes
