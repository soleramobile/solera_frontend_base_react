import React, { useRef, useEffect } from 'react';
import Button from '../../../components/Button';
import TextField from '../../../components/TextField';

const Home: React.FC = () => {
  const inputRef = useRef<HTMLInputElement>(null);

  const handleClick = () => {
    inputRef.current?.focus();
  };

  return (
    <div>
      Home Page
    </div>
  );
}

export default Home;
